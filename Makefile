src = index.rkt
lib = utils.rkt

all: public ethcc2020 cardano2020 avoum2021 secclar2021 shitcoin2022

.PHONY: all public ethcc2020 mrproper clean

public: public/index.html

public/index.html: index.rkt
	racket $< > $@.tmp && mv $@.tmp $@ || rm $@.tmp

ethcc2020: public/ethcc2020/index.html

cardano2020: public/cardano2020/index.html

avoum2021: public/avoum2021/index.html

secclar2021: public/secclar2021/index.html

shitcoin2022: public/shitcoin2022/index.html

mit2022: public/mit2022/index.html

public/%/index.html: %.rkt reveal.rkt
	mkdir -p public/$*
	racket $< > $@.tmp && mv $@.tmp $@ || rm $@.tmp

clean:
	rm -f public/index.html

mrproper:
	git clean -xfd
