#lang at-exp racket @; -*- Scheme -*-
#|
Title: Bitcoin: The Next Smart Contract Platform

Abstract:
Bitcoin today doesn't support arbitrary smart contracts.
But in a few years from now, it could. It can. It will.
Here is the story of how Bitcoin will leapfrog Ethereum and other blockchains
to become the greatest smart contract platform in the Solar System:
Bitcoin script upgrades, native NFTs, arbitrary contracts, Account-View-On-UTXO-Model,
zk-SNARK support, generalized state-channels—and higher-level DApp languages
like Glow (https://glow-lang.org).
It will take several BIPs, that will probably first be adopted by some other Bitcoin clones first. But if it keep moving forward, in three to five years, Bitcoin can be way ahead of where Ethereum is today, and where it may be then.


Author:
François-René Rideau is CEO of [Mutual Knowledge Systems](https://mukn.io)
and author of the DApp language [Glow](https://glow-lang.org)

Venue:
These are notes I took for a talk I gave in Miami at the
[Shitcoin 2022 Conference](https://shitcoinconf.com/)
on Tuesday, April 6th, 2022.

NB: To export to PDF: https://github.com/hakimel/reveal.js#pdf-export

To compile it, use:
   racket shitcoin2022.rkt > public/shitcoin2022/index.html

This document is available under the bugroff license.
   http://tunes.org/legalese/bugroff.html

To test interactively, try:
   racket -i -l scribble/reader -e "(use-at-readtable)" -l racket

- Speak more SLOWWWWLY

|#

(require scribble/html "reveal.rkt")


;; Support for lines of color-annotated source code
(define (spaces n) (make-string n #\uA0))
(define *width* (make-parameter 62))
(define (h-line . elements)
  (let loop ((es elements)
             (hs '())
             (l 0)
             (style identity))
    (match es
      ('() (reverse (list* (br) (style (spaces (- (*width*) l))) hs)))
      (`((,sty . ,str) . ,res) (loop res (cons (sty str) hs) (+ l (string-length str)) sty)))))
(define-syntax line-element (syntax-rules () ((_ (fmt str)) (cons fmt str)) ((_ str) (cons identity str))))
(define-syntax line (syntax-rules () ((_ es ...) (h-line (line-element es) ...))))
(define-syntax source (syntax-rules () ((_ ls ...) (source-div (line . ls) ...))))
(define s+ string-append)

(define code-style "font-family: 'Computer Modern Typewriter', 'courier'; font-weight:normal; font-style:normal; white-space: pre;")
(define source-div-style (s+ "font-size: 4vmin; overflow: auto; overflow-x: hidden; height: 20em; text-align: left; margin-left: auto; margin-right: auto; font: monotype;" code-style))
(define (source-div . x) (div style: source-div-style x))

(define (AtC x) (font style: (s+ code-style "background-color: black;") x))
(define (AtA x) (font style: (s+ code-style "background-color: blue;") x))
(define (AtAC x) (font style: (s+ code-style "background-color: #00008b;") x))
(define (AtB x) (font style: (s+ code-style "background-color: red;") x))
(define (AtBC x) (font style: (s+ code-style "background-color: #8b0000;") x))
(define (code . x) (apply font style: code-style x))

(slide ()
@div[class: 'logo]{
@img[src: "../pics/mukn-name.svg" alt: "Mutual Knowledge Systems" width: "75%" valign: 'middle
    style: "
    vertical-align: middle;
    background-color: white;
    padding-left: .5em;
    padding-right: .5em;
    padding-top: .5em;
    padding-bottom: .5em;
"]}
@; TODO: hide the menu behind a button at the top when on mobile (?)
@div[class: 'title
     style: "color: white; vertical-align: middle; text-align: center; font-size: 10vmin"
     @b{@(br) @(br)}]
  @div[class: 'text ~]
  @C[class: 'subtitle style: "font-weight: bold;"]{
    Bitcoin: The Next Smart Contract Platform
    @(br) @(br)
  }
  @p{@(~)}
  @C{François-René Rideau, President, @(~)@em{Mutual Knowledge Systems} @(br)
     @code[class: 'email]{fare@"@"mukn.io}}
     @C{Shitcoin 2022} @; video: @url{https://youtu.be/XXXXXX}}
  @C{Slides: @url{https://mukn.gitlab.io/shitcoin2022/}}
  @div[style: "font-size: 50%;" (~)]
  @table[style: "text-align: left; padding-left: 0; margin-left: 0; width: 100%; font-size: 60%;"
   (tr @td{@code{PgDn}: next} @td{@code{PgUp}: previous} @td{@code{↑ ↓ ← → ESC ⏎}
   @td{Touchscreen: swipe down until you must swipe right}})])

(define (fragment #:index (index 1) . body)
  (apply span class: 'fragment data-fragment-index: index body))

#|

Bitcoin today doesn't support arbitrary smart contracts.
But in a few years from now, it could. It can. It will.
Here is the story of how Bitcoin will leapfrog Ethereum and other blockchains
to become the greatest smart contract platform in the Solar System:
Bitcoin script upgrades, native NFTs, arbitrary contracts, Account-View-On-UTXO-Model,
zk-SNARK support, generalized state-channels—and higher-level DApp languages
like Glow (https://glow-lang.org).
It will take several BIPs, that will probably first be adopted by some other Bitcoin clones first. But if it keep moving forward, in three to five years, Bitcoin can be way ahead of where Ethereum is today, and where it may be then.
|#

(slide-group "Introduction: Smart Contracts"

(gslide () @h1{Smart Contracts: What?}
  ~
  @L{Interaction between distrusting participants}
  ~
  @L{… Exchanging digital assets}
  ~
  @L{… According to computer-verifiable rules}
  ~
  @C{Automated enforcement to align interests})

(gslide () @h1{Smart Contracts: Not yet, but soon}
  ~
  @L{Not yet: safe, scalable, portable…}
  ~
  @L{We (at MuKn) understand the missing pieces}
  ~
  @L{Soon to a Blockchain near you}
  ~
  @L{Ultimately: on Bitcoin}
  @comment{
  })

(gslide () @h1{Missing features}
 ~
 @L{Portability: many bad, incompatible VMs and languages}
 ~
 @L{Scalability: orders of magnitude behind centralized}
 ~
 @L{Safety: hacks every week, $billions/year}
 ~
 @L{Usability: not for grammy})

(gslide () @h1{Bitcoin: even further behind}
 ~
 @L{Missing opcodes (except on BCH)}
 ~
 @L{No open contracts (need sub-tokens)}
 ~
 @L{Insufficient size limits}
 ~
 @L{Hard to program, inefficient to run})
);slide-group

(slide-group "Smart Contracts: Not Yet"

(gslide () @h1{Scalability}
 ~
 @L{Throughput vs decentralization tradeoff}
 ~
 @L{Latency vs decentralization tradeoff}
 ~
 @L{Efficiency vs decentralization tradeoff})

(gslide () @h1{UTXO vs Balance}
 ~
 @L{UTXO (BTC): + validate past tx in parallel}
 @L{UTXO (BTC): - cannot do open contracts well}
 ~
 @L{Balance (ETH): - months to validate tx sequentially}
 @L{Balance (ETH): + queue future tx on open contracts}
 ~
 @L{¿Porqué no los dos?}
 ~
 @;@fragment{@C{AVOUM: Account View On UTXO Model}}
 @comment{})

);slide-group

(slide-group "Solutions: Back to Basics"

(gslide () @h1{The Glow Language}
 ~
 @C{@url{https://glow-lang.org}}
 ~
 @L{Safety: 10x less code than Ethereum + JS}
 ~
 @L{Portability: Write Once, Run Anywhere}
 ~
 @C{How? Language abstraction. KISS.})

(gslide () @h1{UTXO vs Balance: Both}
 ~
 @L{AVOUM: Account View On UTXO Model}
 @L{Malleable txs, miners rebase on latest UTXO}
 ~
 @L{Check past txs in parallel with UTXO}
 @L{Queue future txs for open contracts}
 ~
 @L{Easy to layer atop UTXO, harder under Balance})

(gslide () @h1{Throughput: Mutual Knowledge Base}
 ~
 @L{Common Knowledge vs Mutual Knowledge}
 ~
 @L{Parallelizable “Court Registry” vs Slow “Court”}
 ~
 @L{“Data Availability Engine” for side-chains})

(gslide () @h1{Latency: Mutual Knowledge Base}
 ~
 @L{Latency: non-custodial Centralized})

(gslide () @h1{Closed Contracts: Generalized State Channels}
 ~
 @L{Contract Goal: never see the judge}
 @L{Commit in the beginning, Settle in the end}
 ~
 @L{State Channels without Lightning Network limitations}
 ~
 @L{Issue: Too hard to program… without Glow}
 @L{Must generate exactly matching on/off chain code})

(gslide () @h1{Efficiency: zkSNARKs}
 ~
 @L{EVM: 1,000,000,000x more expensive, and counting}
 ~
 @L{zkSNARKs: only 1,000,000x, paid once}
 ~
 @L{Can vastly simplify validation of chains and contracts}
 ~
 @L{Issue: Too hard to program… without Glow}
 @L{Must generate exactly matching on/off chain code})

);slide-group
(slide-group "Adding it all to Bitcoin?"


);slide-group
(slide-group "Conclusion: Brave New World"

(gslide () @h1{Smart Contracts: Not yet, but soon}
  ~
  @L{Not yet: safe, scalable, portable…}
  ~
  @L{We (at MuKn) understand the missing pieces}
  ~
  @L{Soon to a Blockchain near you}
  ~
  @L{Ultimately: on Bitcoin}
  @comment{
  })

(gslide () @h1{Improving the Ecosystem}
 ~
 @L{Many One-trick ponies… improve them}
 ~
 @L{Ultimately… improve Bitcoin itself}
 ~
 @L{Reconcile warring factions}
 ~
 @L{Have people listen to each other?})

(gslide () @h1{How did we do it?}
 ~
 @L{Care about the technology}
 ~
 @L{Find the unifying paradigm}
 ~
 @L{Understand fundamentals of CS}
 ~
 @L{Work @em{with}, not against, existing chains})

(gslide () @h1{Take Action}
 @L{Fund us on your chain… or else}
 ~
 @L{Glow Language: @url{https://glow-lang.org}}
 ~
 @L{AVOUM: @url{https://mukn.gitlab.io/avoum2021}}
 ~
 @L{Data Availability Engine, zkSNARKs, TrieZip…}
 ~
 @L{We solve hard problems in Blockchain:}
 @C{@url{https://mukn.io}}
))

(reveal "Bitcoin: The Next Smart Contract Platform")
