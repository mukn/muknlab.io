# Recipes for the following files

## To create the official standalone version of mukn-name or mukn-icon

 * edit the SVG with Inkscape
 * export the SVG as EPS with the "convert to path" option
 * import again in Inkscape with default option
 * export again as SVG

Alternatively, find a way to script the above into existence.


## To rasterize a picture

 * Rasterizing using Imagemagick, keeping the transparent background:
   ```
   convert -density 1200 mukn-name.svg mukn-name.png
   ```

 * Rasterizing using Imagemagick, forcing a white background, with some borders:
   ```
   convert -density 1200 mukn-icon.svg -background white -flatten -bordercolor white -border 60 mukn-icon-whitebg.png
   ```

 * Same as above but with enough borders that GitHub's circle includes the whole picture:
   ```
   convert -density 1200 mukn-icon.svg -background white -flatten -bordercolor white -border 120 mukn-icon-borders.png
   ```

 * Rasterizing using Imagemagick, with a border, so you can insert the picture in a printable document:
   ```
   convert -density 1200 mukn-business-card-back.svg -bordercolor '#cccccc' -border 10 b.png
   ```

 * Making a 1128x191 picture for linkedin (ok, only 1127):
   ```
   convert -density 150 mukn-name.svg -bordercolor 'white' -border 398x8 -matte -mattecolor white -flatten mukn-name.png
   ```

## To convert a picture to grayscale

```
wget https://www.seas.harvard.edu/sites/default/files/styles/embedded_image_large/public/images/news/nada-amin-sq.jpg
convert nada-amin-sq.jpg -set colorspace Gray -separate -average nada-amin-bw.jpg
```

## QR Codes

The QR code files were produced with these commands (`-t png` actually optional):
```
qrencode -o mukn.com.qr.png -t png https://mukn.com
qrencode -o mukn.com__fare.qr.png -t png https://mukn.com/fare
```

Note that you could trim the borders with `convert "${name}.untrimmed.png" -trim "${name}.png"`
but trimmed QR codes don't work well on black background, so better not to trim.

## Print a 3.5"x2" business card at Fedex

I created a new LibreOffice Writer document, used the `Format > Page Style > Page` menu
to change the page size to 3.5"x2" then to set the margins to 0,
then inserted a page break with `Ctrl-Enter`, and finally
used the `Insert > Image` menu to import each of front and back as SVG,
selected everything with `Ctrl-A` (or else only one page would be exported(!))
before to create a PDF with the `File > Export As > Export as PDF...` menu.
Then I could `Upload` the result on the Fedex website for
[Premium Business Cards](https://www.office.fedex.com/default/business-cards-premium.html).

Note that I tried and failed to join PDF files exported separately from each of front and back SVG.
I once wrote a recipe using `pdfjoin` (but which of the many such named program?),
but it's not in NixOS (anymore), and I failed to correctly join PDF files into one
using `pdfjam` from Nix package `texlive-combined-full`:
```
pdfjam -o fare@mukn-business-card.pdf --papersize '{3.5in,2in}' fare@mukn-business-card-front.pdf mukn-business-card-back.pdf
```

## Print a 4"x3" document in a page

In LibreOffice Writer, start a US Letter document, adjust the margins to .25",
and repeatedly `Insert > Image` the SVG for the nametag then adjust its position.
Select everything before to `File > Export As > Export as PDF...` into a PDF.
